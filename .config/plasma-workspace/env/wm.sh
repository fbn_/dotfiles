# Disable KWin and use i3gaps as WM
export KDEWM=bspwm

# Compositor (Animations, Shadows, Transparency)
# xcompmgr -C
#compton -b --vsync --config ~/.config/compton.conf
#compton -b --config /home/fbn/.config/compton.conf
compton -cCzG -t-3 -l-5 -r4 \
 --config /dev/null --backend xr_glx_hybrid \
 --vsync opengl-swc --vsync-use-glfinish \
 --glx-no-stencil --paint-on-overlay \
 --glx-swap-method 3 --glx-no-rebind-pixmap \
 --xrender-sync --xrender-sync-fence \
 --unredir-if-possible &

